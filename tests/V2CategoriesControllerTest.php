<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class V2CategoriesControllerTest extends WebTestCase
{
    public function testGetCategoryWithoutAuth()
    {
        $client = static::createClient();
        $client->request('GET', '/v2/category');
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Invalid credentials."}', $client->getResponse()->getContent());
    }

    public function testGetCategoryWithAuth()
    {
        $client = static::createClient();
        $client->request('GET', '/v2/category', [], [], ['HTTP_apikey' => 'CORS']);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"category":[]}', $client->getResponse()->getContent());
    }

    public function testReadCategory()
    {
        $client = static::createClient();
        $client->request('GET', '/v2/category/1', [], [], ['HTTP_apikey' => 'CORS']);
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Not Found","id":"1"}', $client->getResponse()->getContent());
    }

    public function testReadCategoryError401()
    {
        $client = static::createClient();
        $client->request('GET', '/v2/category/1');
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Invalid credentials."}', $client->getResponse()->getContent());
    }

    public function testReadCategoryError404()
    {
        $client = static::createClient();
        $client->request('GET', '/v2/category/2', [], [], ['HTTP_apikey' => 'CORS']);
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Not Found","id":"2"}', $client->getResponse()->getContent());
    }

    public function testReadCategoryError406()
    {
        $client = static::createClient();
        $client->request('GET', '/v2/category/"1"', [], [], ['HTTP_apikey' => 'CORS']);
        $this->assertEquals(406, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Non numeric value","id":"\"1\""}', $client->getResponse()->getContent());
    }

}

?>
